export interface checkboxOne  {
    id: string;
    type: string;
    label: string;
    required: boolean;
  };

 export interface IColor  {
    id: string;
    type: string;
    label: string;
    required: boolean;
  };

export interface files {
    id: string;
    type: string;
    label: string;
    required: boolean;
    formats: string;
    max_size: number;
    min_size: number;
  };

export interface inputOne  {
    id: string;
    type: string;
    label: string;
    required: boolean;
    placeholder: string;
    maxlength?: number;
    minlength?: number;
    mask?: string;
    pattern?: string;
  };

export interface formDataType  {
    name: string;
    value: string;
  };

export interface select  {
    id: string;
    type: string;
    label: string;
    required: boolean;
    multiple: boolean;
    options: string[];
  };

export  interface textarea {
    id: string;
    type: string;
    label: string;
    required: boolean;
    placeholder: string;
    maxlength?: number;
  };

  interface IButton {
    name:string,
    type: "submit" | "reset" | "button" | undefined
  }
  export interface formjson  {
    form_name: string;
    form_description: string;
    form_fields: Object[];
    form_buttons: IButton[];
  };
  export interface formDataType {
    name: string;
    value: string ;
  };
  export interface arrayForm {
    [key:string]: string | File | boolean
  };