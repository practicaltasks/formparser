import { useEffect, useState } from "react";
import Form from "../components/forms/FormJson";

export default function () {
  let [activeTextError, setTextError] = useState(false);
  let [activeText, setText] = useState(false);
  let [activeDrag, setDrag] = useState(false);
  let [filesList, setFilesList] = useState<File[]>([]);
  let [JsonString, setJsonstring] = useState<string>("");
  let [remove, setRemove] = useState(false);

  useEffect(() => {
    if (remove) {
      setFilesList([]);
    }
  }, [remove]);

  function ChangeFile() {
    let inputFile: HTMLInputElement | null =
      document.querySelector(".ts-file-input");
    if (inputFile !== null) {
      let files = inputFile.files;
      if (files && files[0].size / 1024 > 10) {
        setTextError(true);
      } else {
        setTextError(false);
        let text = document.querySelector(".ts-file-text");
        let spanWeight = document.querySelector(".ts-file-weight");
        let wrapLabel = document.querySelector(".ts-wrap-label");

        if (text !== null && spanWeight !== null && wrapLabel) {
          wrapLabel.setAttribute("style", "display:none");
          if (files) {
            text.textContent = files ? files[0].name.toString() : "";
            filesList.push(files[0]);
            spanWeight.textContent = files
              ? Math.round(files[0].size / 1024).toString() + "MB"
              : "";
            setText(true);
            FileParse();
          }
        }
      }
    }
  }
  function DeleteFile() {
    let spanWeight = document.querySelector(".ts-file-weight");
    let wrapLabel = document.querySelector(".ts-wrap-label");
    if (spanWeight && wrapLabel) {
      setText(false);
      setFilesList([]);
      setRemove(false);
      wrapLabel.setAttribute("style", "display:block");
      spanWeight.textContent = "";
    }
  }
  function DragEnterFile(e: React.DragEvent<HTMLLabelElement>) {
    e.preventDefault();

    let wrapLabel = document.querySelector(".ts-wrap-label");
    if (wrapLabel) {
      setDrag(true);
      wrapLabel.setAttribute("style", "display:none");
    }
  }
  function DragEndFile(e: React.DragEvent<HTMLLabelElement>) {
    e.preventDefault();
    let wrapLabel = document.querySelector(".ts-wrap-label");
    if (wrapLabel) {
      setDrag(false);
      wrapLabel.setAttribute("style", "display:block");
    }
  }
  function DropFile(e: React.DragEvent<HTMLLabelElement>) {
    e.preventDefault();
    let f = [...e.dataTransfer.files];
    filesList.push(...f);
    setDrag(false);
    let text = document.querySelector(".ts-file-text");
    let spanWeight = document.querySelector(".ts-file-weight");
    if (text !== null && spanWeight !== null) {
      text.textContent = f ? f[0].name.toString() : "";
      spanWeight.textContent = f
        ? Math.round(f[0].size / 1024).toString() + "MB"
        : "";
      setText(true);
      FileParse();
    }
  }

  function FileParse() {
    let reader = new FileReader();
    reader.readAsText(filesList[0]);
    reader.onload = function () {
      if (typeof reader.result === "string") setJsonstring(reader.result);
    };

    reader.onerror = function () {
      console.log(reader.error);
    };
  }
  const Clean = () => {
    let spanWeight = document.querySelector(".ts-file-weight");
    let wrapLabel = document.querySelector(".ts-wrap-label");
    if (spanWeight && wrapLabel) {
      setText(false);
      setFilesList([]);
      setJsonstring("");
      setRemove(false);
      wrapLabel.setAttribute("style", "display:block");
      spanWeight.textContent = "";
    }
  };
  return (
    <main className="main">
      <div className="main__container js-container">
        <h1 className="main__title">Динамический парсер форм</h1>
        <label
          htmlFor="input"
          className={activeDrag ? "main__label-active" : "main__label"}
          onChange={ChangeFile}
          onDragEnter={(e) => DragEnterFile(e)}
          onDragLeave={(e) => DragEndFile(e)}
          onDragOver={(e) => DragEnterFile(e)}
          onDropCapture={(e) => DropFile(e)}
        >
          <div className="main__wrap-label ts-wrap-label">
            <input
              id="input"
              type="file"
              className="main__file-input ts-file-input"
            />
            <h2 className="main__file-title">
              <span className="main__span">Select a file</span> or drag in form
            </h2>
            <p className="main__text">
              PNG, jpg, gif files up to 10 MB in size are available for download
            </p>
          </div>

          <p className={activeTextError ? "file__error-active" : "file__error"}>
            The file weight more than 10 MB
          </p>
          <div className="file__wrap-text">
            <p
              className={
                activeText
                  ? "file__text-active"
                  : "file__text " + "ts-file-text"
              }
            ></p>
            <span className="file__weight ts-file-weight"></span>
          </div>

          <button
            className={
              activeText ? "file__btn-delete-active" : "file__btn-delete "
            }
            onClick={DeleteFile}
          >
            Delete file
          </button>
        </label>

        <button type={"button"} className="main__btn" onClick={Clean}>
          Reset
        </button>
        {JsonString !== "" && !remove ? (
          <Form id="form" json={JsonString} remove={setRemove} />
        ) : (
          <p></p>
        )}
      </div>
    </main>
  );
}
