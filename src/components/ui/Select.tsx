import { useEffect, useRef, useState } from "react";
import { select } from "../../functions/Interface";

export default function Select(props: {
  obj: select;
  setRef: React.Dispatch<
    React.SetStateAction<React.MutableRefObject<null> | undefined>
  >;
}) {
  const [active, setActive] = useState(false);
  const [bc, setBc] = useState(props.obj.label);
  const options = props.obj.options;
  const refInput = useRef(null);
  useEffect(() => {
    console.log(refInput)
    props.setRef(refInput);
  }, []);
  return (
    <div className="select">
      <label
        htmlFor=""
        className="select__label"
        onClick={() => setActive(!active)}
        
      >
        <option
          ref={refInput}
          id={props.obj.id}
          className="select__select"
          onChange={() => {}}
          value={bc}
        ></option>
        <p
          className={
            bc === props.obj.label ? "select__p-gray" : "select__current"
          }
        >
          {bc !== props.obj.label ? bc : props.obj.label}
        </p>
        <div className="select__wrap-svg">
          <svg
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            className={
              bc === props.obj.label
                ? "select__disactive-delete"
                : "select__delete"
            }
            onClick={() => {
              setBc(props.obj.label);
            }}
          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M5.13332 10.8556C4.50125 10.8555 3.98887 11.3679 3.98887 12C3.98887 12.6321 4.50126 13.1445 5.13332 13.1445L10.8554 13.1445L10.8554 18.8668C10.8554 19.4989 11.3678 20.0113 11.9999 20.0113C12.632 20.0113 13.1444 19.4989 13.1444 18.8668L13.1443 13.1445L18.8667 13.1445C19.4988 13.1445 20.0112 12.6321 20.0112 12C20.0112 11.3679 19.4988 10.8556 18.8667 10.8556L13.1443 10.8556L13.1443 5.13338C13.1443 4.50132 12.632 3.98893 11.9999 3.98893C11.3678 3.98893 10.8554 4.50131 10.8554 5.13338L10.8554 10.8556L5.13332 10.8556Z"
              fill="#1111117A"
            />
          </svg>

          <svg
            className={active ? "select__arrow-active" : ""}
            width="16"
            height="10"
            viewBox="0 0 16 10"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M7.97055 9.50614C7.77408 9.50728 7.57931 9.46944 7.39742 9.39479C7.21553 9.32015 7.0501 9.21016 6.9106 9.07114L0.939046 3.07114C0.799851 2.93128 0.689436 2.76524 0.614105 2.58251C0.538773 2.39978 0.5 2.20393 0.5 2.00614C0.5 1.80835 0.538773 1.6125 0.614105 1.42976C0.689436 1.24703 0.799851 1.08099 0.939046 0.941137C1.07824 0.801279 1.24349 0.690337 1.42536 0.614647C1.60722 0.538956 1.80215 0.5 1.999 0.5C2.19585 0.5 2.39077 0.538956 2.57264 0.614647C2.75451 0.690337 2.91975 0.801279 3.05895 0.941137L7.97055 5.90614L12.8971 1.13614C13.0344 0.982702 13.2018 0.859389 13.3887 0.77391C13.5756 0.688431 13.7781 0.642625 13.9835 0.639356C14.1888 0.636087 14.3926 0.675425 14.5822 0.754911C14.7717 0.834398 14.9429 0.952323 15.085 1.10131C15.2271 1.2503 15.337 1.42714 15.408 1.62078C15.479 1.81442 15.5094 2.0207 15.4975 2.2267C15.4855 2.4327 15.4313 2.63401 15.3384 2.81802C15.2454 3.00204 15.1157 3.1648 14.9573 3.29614L8.98571 9.08614C8.7127 9.35061 8.34975 9.50077 7.97055 9.50614Z"
              fill="#111111"
              fillOpacity="0.48"
            />
          </svg>
        </div>
      </label>
      <ul
        className={
          active ? "select__ul select__ul-active" : "select__ul-disactive"
        }
      >
        {options.map((e, key) => (
          <li
            key={key}
            className="select__li"
            value={e}
            onClick={() => setBc(e)}
          >
            {e}
            <svg
              className={bc === e ? "" : "select__checked-disactive"}
              width="11"
              height="10"
              viewBox="0 0 11 10"
              fill="black"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M1.71996 5.00006C1.5323 4.8313 1.28778 4.73973 1.03543 4.74373C0.783082 4.74773 0.541579 4.84699 0.359365 5.02162C0.17715 5.19624 0.0677025 5.4333 0.0529749 5.68525C0.0382473 5.9372 0.119329 6.1854 0.279957 6.38006L2.49996 8.71006C2.5929 8.80756 2.70459 8.88528 2.82832 8.93855C2.95204 8.99182 3.08525 9.01955 3.21996 9.02006C3.35395 9.02084 3.48673 8.99469 3.61041 8.94315C3.7341 8.89162 3.84616 8.81575 3.93996 8.72006L10.72 1.72006C10.8119 1.62551 10.8843 1.51378 10.933 1.39124C10.9818 1.26871 11.0059 1.13778 11.004 1.00592C11.0022 0.874059 10.9744 0.743857 10.9222 0.622745C10.87 0.501634 10.7945 0.391986 10.7 0.300061C10.6054 0.208135 10.4937 0.135733 10.3711 0.0869889C10.2486 0.0382443 10.1177 0.0141116 9.98581 0.0159687C9.85396 0.0178259 9.72375 0.0456365 9.60264 0.0978126C9.48153 0.149989 9.37188 0.225509 9.27996 0.320061L3.22996 6.58006L1.71996 5.00006Z"
                fill="black"
              />
            </svg>
          </li>
        ))}
      </ul>
    </div>
  );
}
