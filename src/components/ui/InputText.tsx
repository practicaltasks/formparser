import { useEffect, useRef, useState } from "react";

import { formDataType, inputOne } from "../../functions/Interface";



export default function InputText(props: {
  obj: inputOne;
  Value: Iterable<formDataType>;
  setValue: React.Dispatch<React.SetStateAction<Iterable<formDataType>>>;
  setRef: React.Dispatch<
    React.SetStateAction<React.MutableRefObject<null> | undefined>
  >;
}) {
  const RefInput = useRef(null);
  const [value, setValue] = useState("");
  const [dirty, setDirty] = useState(false);
  const [error, setError] = useState("Поле не может быть пустым");
  const [active, setActive] = useState(false);

  function ValueSet(e: React.ChangeEvent<HTMLInputElement>) {
    setValue(e.target.value);
    setError("");
    props.setRef(RefInput);
    if (props.obj.mask) {
      const ptrn1 =
        /^(0?[1-9]|[12][0-9]|3[01])[- /.](0?[1-9]|1[012])[- /.](19|20)?[0-9]{2}$/;

      if (!ptrn1.test(String(e.target.value))) {
        setError("неверный формат даты");
      }
    }
    if (props.obj.maxlength && props.obj.minlength) {
      if (
        e.target.value.length > props.obj.maxlength ||
        e.target.value.length < props.obj.minlength
      ) {
        setError(
          `Минимальная длина должна быть ${props.obj.minlength} символов, а максимальная ${props.obj.maxlength}`
        );
      } else {
        setError("");
      }
    }
    if (props.obj.pattern) {
      const ptrn = RegExp(props.obj.pattern);
      if (!ptrn.test(String(e.target.value))) {
        if (props.obj.type === "email") setError("email неккоректен");
        if (props.obj.type === "password") setError("пароль неккоректен");
      } else {
        setError("");
      }
    }
  }
  const refLabel = useRef<HTMLLabelElement>(null);
  useEffect(() => {
    if (!active) return;
    function handleClick(e: Event) {
      if (refLabel.current !== null)
        if (!refLabel.current.contains(e.target as HTMLDivElement)) {
          setActive(false);
        }
    }

    document.addEventListener("click", handleClick);
    return () => {
      document.removeEventListener("click", handleClick);
      setDirty(true);
    };
  });
  return (
    <>
      <label
        ref={refLabel}
        htmlFor={props.obj.id}
        className={
          (dirty && error !== "") || (dirty && value === "")
            ? "input-text__label input-text__label--error"
            : "input-text__label"
        }
        onClick={() => setActive(true)}
      >
        <span
          className={
            active || value !== ""
              ? "input-text__span input-text__span--active"
              : "input-text__span"
          }
        >
          {props.obj.label}
        </span>
        <input
          ref={RefInput}
          type={props.obj.type}
          onBlur={
            props.obj.required ? () => setDirty(true) : () => setDirty(false)
          }
          onChange={(e) => ValueSet(e)}
          className={
            (dirty && error !== "") || (dirty && value === "")
              ? "input-text__input input-text__input--error"
              : "input-text__input"
          }
          autoFocus={active}
          maxLength={props.obj.maxlength}
          id={props.obj.id}
          value={value}
        />
      </label>
      {(dirty && error !== "") || (dirty && value === "") ? (
        <p className="input-text__error">
          {error === "" ? "поле не должно быть пустым" : error}
        </p>
      ) : (
        <></>
      )}
    </>
  );
}
