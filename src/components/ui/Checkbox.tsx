import { useEffect, useRef } from "react";
import { checkboxOne } from "../../functions/Interface";

export default function Checkbox(props: { obj: checkboxOne, setRef: React.Dispatch<
  React.SetStateAction<React.MutableRefObject<null> | undefined>
>; }) {
  const refInput = useRef(null);
  useEffect(() => {
    props.setRef(refInput);
  }, []);
  return (
    <div className="checkbox__label">
      <input
      ref={refInput}
        id="check"
        type="checkbox"
        className="checkbox__input"
        required={props.obj.required}
        itemID={props.obj.id}
      />
      <label htmlFor="check" className="checkbox__span"></label>
      <p className="checkbox__text">{props.obj.label}</p>
    </div>
  );
}
