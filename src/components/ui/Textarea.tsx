import { useRef, useState } from "react";
import { textarea } from "../../functions/Interface";

export default function Textarea(props: {
  obj: textarea;
  setRef: React.Dispatch<
    React.SetStateAction<React.MutableRefObject<null> | undefined>
  >;
}) {
  const [dirty, setDirty] = useState(false);
  const [error, setError] = useState("Поле не может быть пустым");
  const [value, setValue] = useState("");
  const refInput = useRef(null);
  function ValueSet(e: React.ChangeEvent<HTMLTextAreaElement>) {
    setValue(e.target.value);
    setError("");
    props.setRef(refInput);
  }
  return (
    <>
      <textarea
        ref={refInput}
        id={props.obj.id}
        className="textarea"
        onBlur={
          props.obj.required ? () => setDirty(true) : () => setDirty(false)
        }
        placeholder={props.obj.placeholder}
        maxLength={props.obj.maxlength}
        value={value}
        onChange={(e) => ValueSet(e)}
      ></textarea>
      {dirty && error !== "" ? (
        <p className="input-text__error">{error}</p>
      ) : (
        <></>
      )}
    </>
  );
}
