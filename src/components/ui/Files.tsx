import { useEffect, useRef, useState } from "react";

import { files } from "../../functions/Interface";

export default function Files(props: {
  obj: files;
  setRef: React.Dispatch<
    React.SetStateAction<React.MutableRefObject<null> | undefined>
  >;
}) {
  const [activeTextFIleErrorFIle, setTextFileErrorFIle] = useState(false);
  const [activeTextFIle, setTextFile] = useState(false);
  const [activeDragFIle, setDragFile] = useState(false);
  const [filesListFile] = useState<File[]>([]);
  const refInput = useRef(null);
  useEffect(() => {
    props.setRef(refInput);
  }, []);
  function Changefile() {
    const inputFile: HTMLInputElement | null = document.querySelector(
      ".ts-file-input-file"
    );
    if (inputFile !== null) {
      const files = inputFile.files;
      if (files && files[0].size / 1024 > 10) {
        setTextFileErrorFIle(true);
      } else {
        setTextFileErrorFIle(false);
        const text = document.querySelector(".ts-file-text-file");
        const spanWeight = document.querySelector(".ts-file-weight-file-file");
        const wrapLabel = document.querySelector(".ts-wrap-label-file");

        if (text !== null && spanWeight !== null && wrapLabel) {
          wrapLabel.setAttribute("style", "display:none");
          if (files) {
            text.textContent = files ? files[0].name.toString() : "";
            filesListFile.push(files[0]);
            spanWeight.textContent = files
              ? Math.round(files[0].size / 1024).toString() + "MB"
              : "";
            setTextFile(true);
            console.log(spanWeight.innerHTML);
          }
        }
      }
    }
  }
  function DeleteFile() {
    const spanWeight = document.querySelector(".ts-file-weight-file-file");
    const wrapLabel = document.querySelector(".ts-wrap-label-file");
    if (spanWeight && wrapLabel) {
      setTextFile(false);
      wrapLabel.setAttribute("style", "display:block");
      spanWeight.textContent = "";
    }
  }
  function DragEnterfile(e: React.DragEvent<HTMLLabelElement>) {
    e.preventDefault();

    const wrapLabel = document.querySelector(".ts-wrap-label-file");
    if (wrapLabel) {
      setDragFile(true);
      wrapLabel.setAttribute("style", "display:none");
    }
  }
  function DragEndfile(e: React.DragEvent<HTMLLabelElement>) {
    e.preventDefault();
    const wrapLabel = document.querySelector(".ts-wrap-label-file");
    if (wrapLabel) {
      setDragFile(false);
      wrapLabel.setAttribute("style", "display:block");
    }
  }
  function DropFile(e: React.DragEvent<HTMLLabelElement>) {
    e.preventDefault();
    const f = [...e.dataTransfer.files];
    filesListFile.push(...f);
    setDragFile(false);
    const text = document.querySelector(".ts-file-text-file");
    const spanWeight = document.querySelector(".ts-file-weight-file");
    if (text !== null && spanWeight !== null) {
      text.textContent = f ? f[0].name.toString() : "";
      spanWeight.textContent = f
        ? Math.round(f[0].size / 1024).toString() + "MB"
        : "";
      setTextFile(true);
      console.log(spanWeight.innerHTML);
    }
  }

  return (
    <label
      htmlFor="input2"
      className={activeDragFIle ? "main__label-active" : "main__label"}
      onChange={Changefile}
      onDragEnter={(e) => DragEnterfile(e)}
      onDragLeave={(e) => DragEndfile(e)}
      onDragOver={(e) => DragEnterfile(e)}
      onDropCapture={(e) => DropFile(e)}
    >
      <div className="main__wrap-label ts-wrap-label-file">
        <input
          ref={refInput}
          id="input2"
          type="file"
          className="main__file-input ts-file-input-file"
          accept={props.obj.formats}
        />
        <h2 className="main__file-title">
          <span className="main__span">Выберете файл</span> or drag in form
        </h2>
        <p className="main__text">
          {`PNG, jpg files up to ${Math.round(
            props.obj.max_size / 1024
          )} MB in size are available for download`}
        </p>
      </div>

      <p
        className={
          activeTextFIleErrorFIle ? "file__error-active" : "file__error"
        }
      >
        {`The file weight more than ${Math.round(
          props.obj.max_size / 1024
        )} MB`}
      </p>
      <div className="file__wrap-text">
        <p
          className={
            activeTextFIle
              ? "file__text-active"
              : "file__text " + "ts-file-text-file"
          }
        ></p>
        <span className="file__weight ts-file-weight-file-file"></span>
      </div>

      <button
        className={
          activeTextFIle ? "file__btn-delete-active" : "file__btn-delete "
        }
        onClick={DeleteFile}
      >
        Delete file
      </button>
    </label>
  );
}
