export default function PrimaryBtn(props: {
  name: string;
  type: "submit" | "reset" | "button" | undefined;
}) {
  return (
    <button  className="primary-btn" form="form" type={props.type}>
      {" "}
      {props.name}
    </button>
  );
}
