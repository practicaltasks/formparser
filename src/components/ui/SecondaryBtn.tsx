export default function SecondaryBtn(props: {
  name: string;
  type: "submit" | "reset" | "button" | undefined;
}) {
  return (
    <button className="secondary-btn" type={props.type}>
      {props.name}
    </button>
  );
}
