import { useEffect, useRef, useState } from "react";
import Checkbox from "../ui/Checkbox";
import CircleBtn from "../ui/CircleBtn";
import Color from "../ui/Color";
import Files from "../ui/Files";
import InputText from "../ui/InputText";
import PrimaryBtn from "../ui/PrimaryBtn";
import SecondaryBtn from "../ui/SecondaryBtn";
import Select from "../ui/Select";
import Textarea from "../ui/Textarea";

import {
  IColor,
  arrayForm,
  checkboxOne,
  files,
  formDataType,
  formjson,
  inputOne,
  select,
  textarea,
} from "../../functions/Interface";
export default function Form(props: {
  id: string;
  json: string;
  remove: React.Dispatch<React.SetStateAction<boolean>>;
}) {
  const [ppActive, setPPActive] = useState(false);
  const [haveInput] = useState<inputOne[] | undefined>([]);
  const [haveSelect] = useState<select[]>([]);
  const [haveTextarea] = useState<textarea[]>([]);
  const [haveCheckbox] = useState<checkboxOne[]>([]);
  const [haveColor] = useState<IColor[]>([]);
  const [haveFile] = useState<files[]>([]);
  const [refArray] = useState<
    React.MutableRefObject<HTMLInputElement & HTMLSelectElement>[]
  >([]);
  const [currentRef, SetCurRef] = useState<
    React.MutableRefObject<any> | undefined
  >();
  const [formDataSubmit, setFormDataSubmit] = useState<Iterable<formDataType>>(
    []
  );
  const [formData, setFormData] = useState<formjson>({
    form_name: "",
    form_description: "",
    form_fields: [],
    form_buttons: [],
  });
  const dataFetch = useRef<boolean>(false);

  useEffect(() => {
    if (currentRef) refArray.push(currentRef);
  }, [currentRef]);

  useEffect(() => {
    if (dataFetch.current) return;
    dataFetch.current = true;
    if (props.json !== "") {
      const json = JSON.parse(props.json);
      setFormData(json);

      for (let elem of json.form_fields) {
        switch (elem.type) {
          case "file":
            haveFile?.push(elem);
            break;
          case "color":
            haveColor?.push(elem);
            break;
          case "text":
            if (haveInput) haveInput.push(elem);
            break;
          case "textarea":
            haveTextarea?.push(elem);
            break;
          case "password":
            haveInput?.push(elem);
            break;
          case "select":
            haveSelect?.push(elem);
            break;
          case "checkbox":
            haveCheckbox?.push(elem);
            break;
        }
      }
    }
  }, []);

  function submit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    setPPActive(true);
    const arrayForm: arrayForm = {};
    for (let i of refArray) {
      if (i.current.files) {
        const key1 = i.current.id;

        arrayForm[key1] = i.current.files[0];
      } else {
        const key1 = i.current.id;

        arrayForm[key1] = i.current.value || i.current.checked;
      }
    }

    console.log(arrayForm);
  }
  function RemoveForm() {
    props.remove(true);
  }

  return (
    <div className="form__wrap">
      <div className="form__btn-close" onClick={RemoveForm}>
        <CircleBtn />
      </div>

      <h2 className="form__title">{formData.form_name}</h2>
      <p className="form__description">{formData.form_description}</p>

      <form onSubmit={(e) => submit(e)} action="" id="form" className="form">
        {haveCheckbox?.length !== 0 ? (
          haveCheckbox?.map((e, key) => (
            <Checkbox obj={e} key={key} setRef={SetCurRef} />
          ))
        ) : (
          <></>
        )}
        {haveInput?.length !== 0 ? (
          haveInput?.map((e, key) => (
            <InputText
              setRef={SetCurRef}
              key={key}
              Value={formDataSubmit}
              setValue={setFormDataSubmit}
              obj={e}
            />
          ))
        ) : (
          <></>
        )}
        {haveSelect?.length !== 0 ? (
          haveSelect?.map((e, key) => (
            <Select setRef={SetCurRef} obj={e} key={key} />
          ))
        ) : (
          <></>
        )}
        {haveTextarea?.length !== 0 ? (
          haveTextarea?.map((e, key) => (
            <Textarea setRef={SetCurRef} obj={e} key={key} />
          ))
        ) : (
          <></>
        )}
        {haveColor?.length !== 0 ? (
          haveColor?.map((e, key) => (
            <Color setRef={SetCurRef} obj={e} key={key} />
          ))
        ) : (
          <></>
        )}
        {haveFile?.length !== 0 ? (
          haveFile?.map((e, key) => (
            <Files setRef={SetCurRef} obj={e} key={key} />
          ))
        ) : (
          <></>
        )}
        <div defaultValue="btn" className="form__btn-wrap">
          {formData?.form_buttons?.length < 2 ? (
            <></>
          ) : (
            <PrimaryBtn
              name={formData.form_buttons[1].name}
              type={formData.form_buttons[1].type}
            />
          )}
          <SecondaryBtn
            name={formData?.form_buttons[0]?.name}
            type={formData?.form_buttons[0]?.type}
          />
        </div>
      </form>
    </div>
  );
}
